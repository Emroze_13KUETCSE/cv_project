package com.emroze.abdullah.cv_project;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ProgressBar;

import com.emroze.abdullah.cv_project.Controller.HistogramAdapter;
import com.emroze.abdullah.cv_project.Model.HistogramItem;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/Abdullah_Emroze_CVs/";
    public static final String lang = "eng";
    private static final String TAG = "CVs";
    private static final int REQUEST_STORAGE_PERMISSIONS_CODE=101;
    protected Button buttonGenerateOutput;
    private Bitmap sourceBitmapPage1,sourceBitmapPage2,sourceBitmapPage3;
    protected ProgressBar progressBar;
    ArrayList<HistogramItem> histogramItems = new ArrayList<>();
    HistogramAdapter histogramAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initParam();
        requestStorsge();
        initData();

        sourceBitmapPage1 = BitmapFactory.decodeResource(getResources(), R.drawable.cv_1);
        sourceBitmapPage2 = BitmapFactory.decodeResource(getResources(), R.drawable.cv_2);
        sourceBitmapPage3 = BitmapFactory.decodeResource(getResources(), R.drawable.cv_3);

    }

    @Override
    protected void onResume() {
        super.onResume();

        buttonGenerateOutput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanAsyncTask scanAsyncTask = new ScanAsyncTask();
                scanAsyncTask.setProgressbar(progressBar);
                scanAsyncTask.execute();
            }
        });
    }

    /**
     * The method declaration for initiate the params.
     */
    private void initParam(){
        buttonGenerateOutput = findViewById(R.id.btn_generate);
        progressBar = findViewById(R.id.progress_circular);
        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
    }

    /**
     * The method declaration for initiate the Model data for histogram.
     */
    private void initData(){
        histogramItems.add(new HistogramItem('0',0));
        histogramItems.add(new HistogramItem('1',0));
        histogramItems.add(new HistogramItem('2',0));
        histogramItems.add(new HistogramItem('3',0));
        histogramItems.add(new HistogramItem('4',0));
        histogramItems.add(new HistogramItem('5',0));
        histogramItems.add(new HistogramItem('6',0));
        histogramItems.add(new HistogramItem('7',0));
        histogramItems.add(new HistogramItem('8',0));
        histogramItems.add(new HistogramItem('9',0));

        histogramItems.add(new HistogramItem('a',0));
        histogramItems.add(new HistogramItem('b',0));
        histogramItems.add(new HistogramItem('c',0));
        histogramItems.add(new HistogramItem('d',0));
        histogramItems.add(new HistogramItem('e',0));
        histogramItems.add(new HistogramItem('f',0));
        histogramItems.add(new HistogramItem('g',0));
        histogramItems.add(new HistogramItem('h',0));
        histogramItems.add(new HistogramItem('i',0));
        histogramItems.add(new HistogramItem('j',0));
        histogramItems.add(new HistogramItem('k',0));
        histogramItems.add(new HistogramItem('l',0));
        histogramItems.add(new HistogramItem('m',0));
        histogramItems.add(new HistogramItem('n',0));
        histogramItems.add(new HistogramItem('o',0));
        histogramItems.add(new HistogramItem('p',0));
        histogramItems.add(new HistogramItem('q',0));
        histogramItems.add(new HistogramItem('r',0));
        histogramItems.add(new HistogramItem('s',0));
        histogramItems.add(new HistogramItem('t',0));
        histogramItems.add(new HistogramItem('u',0));
        histogramItems.add(new HistogramItem('v',0));
        histogramItems.add(new HistogramItem('w',0));
        histogramItems.add(new HistogramItem('x',0));
        histogramItems.add(new HistogramItem('y',0));
        histogramItems.add(new HistogramItem('z',0));

    }

    /**
     * The method declaration for initiate the params.
     */
    public void init(){
        String[] paths = new String[] { DATA_PATH, DATA_PATH + "tessdata/" };

        for (String path : paths) {
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    Log.v(TAG, "ERROR: Creation of directory " + path + " on sdcard failed");
                    return;
                } else {
                    Log.v(TAG, "Created directory " + path + " on sdcard");
                }
            }

        }


        if (!(new File(DATA_PATH + "tessdata/" + lang + ".traineddata")).exists()) {
            try {

                AssetManager assetManager = getAssets();
                InputStream in = assetManager.open("testdata/" + lang + ".traineddata");
                OutputStream out = new FileOutputStream(DATA_PATH
                        + "tessdata/" + lang + ".traineddata");

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();

                Log.v(TAG, "Copied " + lang + " traineddata");
            } catch (IOException e) {
                Log.e(TAG, "Was unable to copy " + lang + " traineddata " + e.toString());
            }
        }
    }

    /**
     * The method declaration for request the file storage permission.
     */
    public void requestStorsge(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(getApplicationContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSIONS_CODE);
            }
            else {
                init();
            }

        } else {
            init();
        }
    }

    @Override
    public void onRequestPermissionsResult(int reqCode, String[] perms, int[] results) {
        if (reqCode == REQUEST_STORAGE_PERMISSIONS_CODE) {
            if (results.length > 0 && results[0] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    init();
                }
                else {
                    requestStorsge();
                }


            }
        }

    }

    /**
     * The AsyncTask declaration for OCR. This take a bitmap object
     * then process it and generate the Text output.
     *
     */
    class ScanAsyncTask extends AsyncTask<Void,Void,Boolean> {

        ProgressBar circleProgressBar;
        int bigger=0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        @Override
        protected void onProgressUpdate(Void...values) {
            super.onProgressUpdate();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            Bitmap bitmap1 = sourceBitmapPage1.copy(Bitmap.Config.ARGB_8888, true);
            Bitmap bitmap2 = sourceBitmapPage2.copy(Bitmap.Config.ARGB_8888, true);
            Bitmap bitmap3 = sourceBitmapPage3.copy(Bitmap.Config.ARGB_8888, true);


            TessBaseAPI baseApi = new TessBaseAPI();
            baseApi.setDebug(true);
            baseApi.init(DATA_PATH, lang);

            //extract text from image1
            baseApi.setImage(bitmap1);
            String recognizedText = baseApi.getUTF8Text();

            //extract text from image2
            baseApi.setImage(bitmap2);
            recognizedText = recognizedText+baseApi.getUTF8Text();

            //extract text from image3
            baseApi.setImage(bitmap3);
            recognizedText = recognizedText+baseApi.getUTF8Text();

            baseApi.end();

            if ( lang.equalsIgnoreCase("eng") ) {
                recognizedText = recognizedText.replaceAll("[^a-zA-Z0-9\n]+",  " ");
            }


            for(int i = 0 ; i < histogramItems.size();i++){
                int count = countChar(recognizedText.toLowerCase(),histogramItems.get(i).getLetter());
                histogramItems.get(i).setLetterCount(count);
                //find the largerst value for histogram analysis
                if(count >= bigger){
                    bigger = count;
                }
            }

            Log.v(TAG, " Output : \n");

            for(int i =0 ; i < histogramItems.size(); i++){
                HistogramItem item = histogramItems.get(i);
                int dashCount = (item.getLetterCount()*80)/bigger;
                String dash = "";
                for(int j = 0 ; j < dashCount; j++){
                    dash = dash+"-";
                }
                Log.v(TAG, String.valueOf(item.getLetter())+": "+dash);

            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            this.circleProgressBar.setVisibility(View.INVISIBLE);

            histogramAdapter = new HistogramAdapter(histogramItems,bigger,getApplicationContext());
            recyclerView.setAdapter(histogramAdapter);
        }

        private int countChar(String str, char c) {
            int count = 0;

            for(int i=0; i < str.length(); i++)
            {    if(str.charAt(i) == c)
                count++;
            }

            return count;
        }


    }
}
