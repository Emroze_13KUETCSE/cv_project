package com.emroze.abdullah.cv_project.Controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emroze.abdullah.cv_project.Model.HistogramItem;
import com.emroze.abdullah.cv_project.R;

import java.util.ArrayList;

public class HistogramAdapter extends RecyclerView.Adapter<HistogramAdapter.ViewHolder>{

    public ArrayList<HistogramItem> histogramItems;
    private Context context;
    private int bigger=0;

    public HistogramAdapter(ArrayList<HistogramItem> histogramItems,int bigger, Context context){
        this.histogramItems= histogramItems;
        this.context = context;
        this.bigger = bigger;
    }

    /**
     * The method declaration for set Histogram Data. This method will be called
     * to set data.
     *
     * @param list
     */
    public void setItemList(ArrayList<HistogramItem> list) {
        this.histogramItems = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.histogram_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final HistogramAdapter.ViewHolder holder, final int position) {
        HistogramItem item = histogramItems.get(position);
        int dashCount = (item.getLetterCount()*80)/bigger;
        String dash = "";
        for(int i = 0 ; i < dashCount; i++){
            dash = dash+"-";
        }

        holder.tvCount.setText(dash);
        holder.tvChar.setText(String.valueOf(item.getLetter())+": ");
    }

    @Override
    public int getItemCount() {
        if (histogramItems != null) {
            return histogramItems.size();
        } else {
            return 0;
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvChar,tvCount;

        protected ViewHolder(View view) {
            super(view);

            tvChar = view.findViewById(R.id.tv_char);
            tvCount = view.findViewById(R.id.tv_count);

        }
    }
}


