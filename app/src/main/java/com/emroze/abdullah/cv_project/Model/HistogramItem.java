package com.emroze.abdullah.cv_project.Model;

public class HistogramItem {
    char letter;
    int letterCount;

    public HistogramItem() {
    }

    public HistogramItem(char letter, int letterCount) {
        this.letter = letter;
        this.letterCount = letterCount;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public int getLetterCount() {
        return letterCount;
    }

    public void setLetterCount(int letterCount) {
        this.letterCount = letterCount;
    }
}
